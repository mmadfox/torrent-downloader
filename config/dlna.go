package config

type DLNA struct {
	RootPath            string
	InterfaceName       string
	HttpPort            int
	ServerName          string
	LogHeaders          bool
	NoTranscode         bool
	NoProbe             bool
	ForceTranscodeTo    string
	AllowedIpNets       string
	ProbeCachePath      string
	Icon                string
	StallEventSubscribe bool
	NotifyInterval      int
	IgnoreHidden        bool
	IgnoreUnreadable    bool
}

func DefaultDLNA() DLNA {
	return DLNA{
		ProbeCachePath: "/tmp/.xtd-ffprobe-cache",
		HttpPort:       1338,
		RootPath:       "~/dlna",
		NotifyInterval: 30,
		IgnoreHidden:   true,
		ServerName:     "XTD Media server",
		NoTranscode:    false,
	}
}
