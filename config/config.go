package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	RootPath string

	DLNA    DLNA
	Torrent Torrent
	Ws      Ws
}

func New() (*Config, error) {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/xtd/")
	viper.AddConfigPath("$HOME/.xtd")
	viper.AddConfigPath(".")

	conf := &Config{
		DLNA:    DefaultDLNA(),
		Torrent: Torrent{},
		Ws:      Ws{},
	}

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}
	if err := viper.Unmarshal(conf); err != nil {
		return nil, err
	}

	conf.DLNA.RootPath = conf.RootPath

	return conf, nil
}
