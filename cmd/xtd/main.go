package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/mmadfox/torrent-downloader/dlna"

	"gitlab.com/mmadfox/torrent-downloader/config"
	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	sugar := logger.Sugar()

	conf, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	dlnaServer, err := dlna.NewServer(
		conf.DLNA,
		sugar.Named("dlna"),
	)
	if err != nil {
		sugar.Fatal(err)
	}
	dlnaServer.Run()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM)
	<-sigs

	dlnaServer.Stop()
}
