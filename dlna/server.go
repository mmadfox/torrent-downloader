package dlna

import (
	"fmt"
	"net"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/anacrolix/dms/rrcache"

	"github.com/anacrolix/dms/dlna/dms"
	"gitlab.com/mmadfox/torrent-downloader/config"
	"go.uber.org/zap"
)

type Server struct {
	logger    *zap.SugaredLogger
	dlna      *dms.Server
	cache     *fFprobeCache
	cachePath string
}

func NewServer(c config.DLNA, logger *zap.SugaredLogger) (*Server, error) {
	iface, err := makeIface(c.InterfaceName)
	if err != nil {
		return nil, err
	}
	conn, err := makeHTTPConn(c.HttpPort)
	if err != nil {
		return nil, err
	}
	cache := &fFprobeCache{
		c:     rrcache.New(64 << 20),
		Mutex: sync.Mutex{},
	}
	if err := cache.load(c.ProbeCachePath); err != nil {
		logger.Warn("FFProbe load cache", err)
	}
	dlna := &dms.Server{
		Interfaces:       iface,
		HTTPConn:         conn,
		FriendlyName:     c.ServerName,
		RootObjectPath:   filepath.Clean(c.RootPath),
		FFProbeCache:     cache,
		LogHeaders:       c.LogHeaders,
		NoTranscode:      c.NoTranscode,
		ForceTranscodeTo: c.ForceTranscodeTo,
		NoProbe:          c.NoProbe,
		AllowedIpNets:    makeIpNets(c.AllowedIpNets),
		Icons: []dms.Icon{
			dms.Icon{
				Width:      48,
				Height:     48,
				Depth:      8,
				Mimetype:   "image/png",
				ReadSeeker: readIcon(c.Icon, 48),
			},
			dms.Icon{
				Width:      128,
				Height:     128,
				Depth:      8,
				Mimetype:   "image/png",
				ReadSeeker: readIcon(c.Icon, 128),
			},
		},
		StallEventSubscribe: c.StallEventSubscribe,
		NotifyInterval:      time.Duration(c.NotifyInterval) * time.Second,
		IgnoreHidden:        c.IgnoreHidden,
		IgnoreUnreadable:    c.IgnoreUnreadable,
	}
	if err := dlna.Init(); err != nil {
		return nil, err
	}
	return &Server{
		logger:    logger,
		dlna:      dlna,
		cache:     cache,
		cachePath: c.ProbeCachePath,
	}, nil
}

func (s *Server) Run() {
	go func() {
		if err := s.dlna.Run(); err != nil {
			s.logger.Fatal(err)
		}
	}()
}

func (s *Server) Stop() {
	if err := s.dlna.Close(); err != nil {
		s.logger.Error("DLNA server stop", err)
	}
	if err := s.cache.save(s.cachePath); err != nil {
		s.logger.Warn("FFProbe save cache", err)
	}
}

func makeIface(ifName string) (ifs []net.Interface, err error) {
	if ifName == "" {
		ifs, err = net.Interfaces()
	} else {
		var if_ *net.Interface
		if_, err = net.InterfaceByName(ifName)
		if if_ != nil {
			ifs = append(ifs, *if_)
		}
	}
	if err != nil {
		return nil, err
	}
	var tmp []net.Interface
	for _, if_ := range ifs {
		if if_.Flags&net.FlagUp == 0 || if_.MTU <= 0 {
			continue
		}
		tmp = append(tmp, if_)
	}
	ifs = tmp
	return
}

func makeHTTPConn(addr int) (net.Listener, error) {
	conn, err := net.Listen("tcp", fmt.Sprintf(":%d", addr))
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func makeIpNets(s string) []*net.IPNet {
	var nets []*net.IPNet
	if len(s) < 1 {
		_, ipnet, _ := net.ParseCIDR("0.0.0.0/0")
		nets = append(nets, ipnet)
		_, ipnet, _ = net.ParseCIDR("0:0:0::/128")
		nets = append(nets, ipnet)
	} else {
		for _, el := range strings.Split(s, ",") {
			ip := net.ParseIP(el)
			if ip == nil {
				_, ipnet, err := net.ParseCIDR(el)
				if err == nil {
					nets = append(nets, ipnet)
				}

			} else {
				_, ipnet, err := net.ParseCIDR(el + "/32")
				if err == nil {
					nets = append(nets, ipnet)
				}
			}
		}
	}
	return nets
}
